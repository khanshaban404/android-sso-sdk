To use the feature first initialise the library as

1. Add jitpack.io as a repository inside dependencyResolutionManagement in settings.gradle
   If your RepositoriesMode is set to FAIL_ON_PROJECT_REPOS
```
dependencyResolutionManagement {
    repositoriesMode.set(RepositoriesMode.FAIL_ON_PROJECT_REPOS)
    repositories {
        google()
        mavenCentral()
        maven { url 'https://jitpack.io' }
    }
}
```  
2. Else, If your repositoriesMode is set to PREFER_SETTINGS inside dependencyResolutionManagement in settings.gradle, 
   Add the below code snippet in your root build.gradle at the end of repositories
```
allprojects {
		repositories {
			...
			maven { url 'https://jitpack.io' }
		}
	}
``` 
3. Implement library as
```
implementation 'com.gitlab.shashwat-vik:android-sso-sdk:1.2'
```

4. Allow the internet permission in Manifest of your project as
```
<uses-permission android:name="android.permission.INTERNET"/>
```

5. Add the Web activity of module CipherText in your Manifest.xml as
 ```
 <activity android:name="com.edugorilla.ssologin.WebView"/>
 ```
6. Create new folder "raw" in resource files and copy the binary secret file provided by edugorilla in raw folder

7. Call the function below on the desired view click listener as:

```
EdugorillaSSO.initializeBaseUrlAndFileLocation(base_url, getResources().getIdentifier("name_of_the binary_secret_file", "raw", getApplicationContext().getPackageName()));
```

8. Create new JsonObject variable "user_info" which will consist of user credentials in the manner stated below :

```
    JSONObject user_info = new JSONObject();
    user_info.put("name", <String>);
    user_info.put("email", <String>);
    user_info.put("mobile", <String>); *mobile number should not exceed more than 10 digits
```

9. And then call the EdugorillaSSO.encryptUrlAndOpenWebView(context, user_info, redirect_url);

```
redirect_url = The URL of the page the user have to land after auto logging in 
```