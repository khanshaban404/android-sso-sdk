package com.edugorilla.ssologin;


import android.content.Context;
import android.content.Intent;

import android.net.Uri;
import android.util.Base64;

import com.google.androidbrowserhelper.trusted.TwaLauncher;


import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

public class EdugorillaSSO {

    static private String base_url;
    static private int secret_key_location;

    public static void initializeBaseUrlAndFileLocation(String client_base_url, int aes_secret_key_location) {
        // Remove any forward slash if user passed a base url with it
        if (String.valueOf(client_base_url.charAt(client_base_url.length() - 1)).equals("/")) {
            base_url = client_base_url.substring(0, client_base_url.length() - 2);
        } else {
            base_url = client_base_url;
        }
        secret_key_location = aes_secret_key_location;
    }

    public static void encryptUrlAndOpenWebView(Context context, String user_info, String redirect_url) throws Exception {
        // Get user info and private key for encryption
        byte[] private_key = getPrivateKeyData(context);
        ArrayList<String> cipher_and_iv_text = encrypt(user_info, private_key);
        String full_url = base_url + "/api/v1/auth/crypt_cbc_login?ct=" + cipher_and_iv_text.get(0) + "&iv=" + cipher_and_iv_text.get(1) + "&r_url=" + redirect_url;
        // Open the created url in the web view
        Intent intent = new Intent(context, WebView.class);
        intent.putExtra("url", full_url);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

    public static void encryptUrlAndOpenTwa(Context context, String user_info, String redirect_url, String token) throws Exception {
        byte[] private_key = stringToByte(token);
        ArrayList<String> cipher_and_iv_text = encrypt(user_info, private_key);
        String full_url = base_url + "/api/v1/auth/crypt_cbc_login?ct=" + cipher_and_iv_text.get(0) + "&iv=" + cipher_and_iv_text.get(1) + "&user_token=" + token + "&r_url=" + redirect_url;
        Uri LAUNCH_URI = Uri.parse(full_url);
        TwaLauncher launcher = new TwaLauncher(context);
        launcher.launch(LAUNCH_URI);
    }

    private static byte[] getPrivateKeyData(Context context) throws IOException {
        InputStream ins = context.getResources().openRawResource(secret_key_location);
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        int size;
        byte[] buffer = new byte[1024];
        while ((size = ins.read(buffer, 0, 1024)) >= 0) {
            outputStream.write(buffer, 0, size);
        }
        ins.close();
        buffer = outputStream.toByteArray();
        return buffer;
    }

    private static String base64Encode(byte[] byte_text) {
        return Base64.encodeToString(byte_text, Base64.NO_WRAP);
    }

    private static byte[] stringToByte(String text) {
        try {
            return (text).getBytes("UTF-8");
        } catch (Exception e) {
            return new byte[0];
        }
    }

    public static ArrayList<String> encrypt(String data, byte[] key) throws Exception {
        byte [] plain_text = stringToByte(data);
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        SecretKeySpec key_spec = new SecretKeySpec(key, "AES");
        cipher.init(Cipher.ENCRYPT_MODE, key_spec);
        byte[] cipherText = cipher.doFinal(plain_text);
        byte[] iv_text = cipher.getIV();
        ArrayList<String> arr = new ArrayList<>();
        arr.add(base64Encode(cipherText));
        arr.add(base64Encode(iv_text));
        return arr;
    }
}